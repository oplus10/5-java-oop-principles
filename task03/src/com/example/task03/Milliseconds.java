package com.example.task03;

/**
 * Интервал в миллисекундах
 */
public class Milliseconds implements TimeUnit {

    private final long amount;

    public Milliseconds(long amount) {
        this.amount = amount;
    }

    @Override
    public long toMillis() {
        return amount;
    }

    @Override
    public long toSeconds() {
        return Math.toIntExact(Math.round((double) amount / 1000));
    }

    @Override
    public long toMinutes() {
        return Math.toIntExact(Math.round((double) amount / 60000));
    }

    @Override
    public long getHours() {
        return Math.toIntExact(Math.round((double) amount / 3600000));
    }
}
