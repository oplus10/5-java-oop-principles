package com.example.task01;

public class Task01Main {
    public static void main(String[] args) {
        Logger logger1 = new Logger("logger1");
        Logger logger2 = new Logger("logger2");
        Logger logger3 = Logger.getLogger("logger1");
        if (logger1 == logger3)
            System.out.println("TRUE\n");
        else
            System.out.println("FALSE\n");

        logger1.log(Log.Level.ERROR, "test message :((");
    }//[ERROR] YYYY.MM.DD hh:mm:ss test - test message
}
