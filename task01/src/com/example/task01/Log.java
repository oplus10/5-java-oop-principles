package com.example.task01;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log {
    public enum Level {DEBUG, INFO, WARNING, ERROR}

    private String strRepresent;
    private LocalDateTime timestamp;
    private Level level;

    public Log(Level lvl, LocalDateTime date, String message, String loggerName) {
        this.level = lvl;
        this.strRepresent = formatLogMessage(lvl, message, loggerName);
        this.timestamp = date;
    }

    private String formatLogMessage(Log.Level level, String msg, String loggerName) {
        LocalDateTime currentTime = LocalDateTime.now();

        String lvl = getLevelStr();
        String date = getTimestamp().substring(0, 10);
        String time = getTimestamp().substring(11);
        String message = getStrRepresent();

        String tmp = "[" + lvl + "] " + date + " " + time + " " + loggerName + " - " + message;
        return tmp;
    }

    public Level getLevel(){
        return level;
    }

    public String getLevelStr() {
        switch (level) {
            case DEBUG:
                return "DEBUG";
            case INFO:
                return "INFO";
            case WARNING:
                return "WARNING";
            case ERROR:
                return "ERROR";
            default:
                return "NONE";
        }
    }

    public String getTimestamp() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");
        return timestamp.format(formatter);
    }

    public String getStrRepresent() {
        return strRepresent;
    }
}
