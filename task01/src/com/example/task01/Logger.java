package com.example.task01;

import java.lang.NullPointerException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDateTime;

public class Logger {
    private static Map<String, Logger> loggerMap = new HashMap<>();
    private ArrayList<Log> logs = new ArrayList<>();
    private String name;

    Log.Level currentLevel;

    public Logger(String name) {
        if (name == null)
            throw new NullPointerException("Invalid parameter");
        if (loggerMap.containsKey(name)) {
            throw new RuntimeException("An instance of the class already exists, creating a new object is not possible");
        } else {
            this.name = name;
            this.currentLevel = Log.Level.DEBUG;
            loggerMap.put(this.name, this);
        }
    }

    public Logger(String name, Log.Level lvl) {
        if (name == null || lvl == null)
            throw new NullPointerException("Invalid parameter");
        this.name = name;
        this.currentLevel = lvl;
    }

    public String getName() {
        return name;
    }

    public static Logger getLogger(String name) {
        if (name == null)
            throw new NullPointerException("Invalid parameter");
        if (loggerMap.containsKey(name)) {
            return loggerMap.get(name);
        } else {
            Logger logger = new Logger(name);
            loggerMap.put(name, logger);
            return logger;
        }
    }

    public void debug(String message) {
        log(Log.Level.DEBUG, message);
    }

    public void debug(String message, Object... args) {
        log(Log.Level.DEBUG, message, args);
    }

    public void info(String message) {
        log(Log.Level.INFO, message);
    }

    public void info(String message, Object... args) {
        log(Log.Level.INFO, message, args);
    }

    public void warning(String message) {
        log(Log.Level.WARNING, message);
    }

    public void warning(String message, Object... args) {
        log(Log.Level.WARNING, message, args);
    }

    public void error(String message) {
        log(Log.Level.ERROR, message);
    }

    public void error(String message, Object... args) {
        log(Log.Level.ERROR, message, args);
    }

    public void log(Log.Level lvl, String message) {
        if (lvl == null)
            throw new NullPointerException("Invalid parameter");
        LocalDateTime currentTime = LocalDateTime.now();
        Log log = new Log(lvl, currentTime, message, this.name);
        logs.add(log);
    }

    public void log(Log.Level lvl, String message, Object... args) {
        if (lvl == null)
            throw new NullPointerException("Invalid parameter 'lvl'");
        String formattedMessage = String.format(message, args);
        LocalDateTime currentTime = LocalDateTime.now();
        Log log = new Log(lvl, currentTime, formattedMessage, this.name);
        logs.add(log);
    }

    public void setLevel(Log.Level lvl) {
        if (lvl == null)
            throw new NullPointerException("Invalid parameter");
        currentLevel = lvl;
    }

    public Log.Level getLevel() {
        return currentLevel;
    }
}