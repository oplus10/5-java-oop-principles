package com.example.task02;

import java.lang.IllegalArgumentException;

public class DiscountBill extends Bill {

    private int discount;

    public int GetDiscount() {
        return discount;
    }

    public void SetDiscount(int discount) {
        if (discount < 0 || discount > 100) {
            throw new IllegalArgumentException("Incorrect discount value!");
        }

        this.discount = discount;
    }

    @Override
    public long getPrice() {
        long price = super.getPrice();

        if (this.discount == 0)
            return price;

        return price - price*(this.discount/100);
    }

    public int getDiscount(){
        return this.discount;
    }

    public long getAbsDiscount(){
        return super.getPrice() - this.getPrice();
    }

}
