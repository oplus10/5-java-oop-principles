package com.example.task04;

import java.util.List;
import java.util.ArrayList;

public class MemoryHandler implements MessageHandler {
    private List<String> memoryBuffer;
    private int memBufSize;
    private MessageHandler rootHandler;

    public MemoryHandler(int bufferSize, MessageHandler rootHandler) {
        this.memoryBuffer = new ArrayList<>();
        this.memBufSize = bufferSize;
        this.rootHandler = rootHandler;
    }

    @Override
    public void handleLogMessage(String formattedMessage) {
        memoryBuffer.add(formattedMessage);
        if (memoryBuffer.size() >= memBufSize) {
            write();
        }
    }

    public void write() {
        for (String message : memoryBuffer) {
            rootHandler.handleLogMessage(message);
        }
        memoryBuffer.clear();
    }
}