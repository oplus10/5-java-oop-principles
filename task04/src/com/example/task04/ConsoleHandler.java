package com.example.task04;

public class ConsoleHandler implements MessageHandler {
    @Override
    public void handleLogMessage(String formattedMessage) {
        System.out.println(formattedMessage);
    }
}