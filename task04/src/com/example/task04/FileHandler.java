package com.example.task04;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class FileHandler implements MessageHandler {
    private String filename;

    public FileHandler(String filename) {
        this.filename = filename;
    }

    @Override
    public void handleLogMessage(String formattedMessage) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(filename, true))) {
            writer.println(formattedMessage);
        } catch (IOException e) {
            throw new RuntimeException("Error while writing to file: ", e);
        }
    }
}

