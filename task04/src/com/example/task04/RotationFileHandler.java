package com.example.task04;

import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class RotationFileHandler implements MessageHandler {
    private final String path;
    private LocalDateTime lastTimePoint;
    private final long rotationTime;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");

    public RotationFileHandler(String path, long rotationTime) {
        this.path = path;
        this.rotationTime = rotationTime;
        this.lastTimePoint = LocalDateTime.now();
    }

    @Override
    public void handleLogMessage(String formattedMessage) {
        try {
            rotateIfNeeded();
        }catch (IOException e){
            throw new RuntimeException("Error while writing to file: ", e);
        }
        try (PrintWriter writer = new PrintWriter(new FileWriter(getCurrentFilename(), true))) {
            writer.println(formattedMessage);
        } catch (IOException e) {
            throw new RuntimeException("Error while writing to file: ", e);
        }
    }

    private String getCurrentFilename() {
        LocalDateTime currentTime = LocalDateTime.now();
        return path + "-" + currentTime.format(formatter) + ".log";
    }

    private void rotateIfNeeded() throws IOException {
        LocalDateTime currentTime = LocalDateTime.now();
        if (currentTime.minusSeconds(rotationTime).isAfter(lastTimePoint)) {
            lastTimePoint = currentTime;
            String currentFilename = getCurrentFilename();
            try {
                PrintWriter writer = new PrintWriter(new FileWriter(currentFilename, true));
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}