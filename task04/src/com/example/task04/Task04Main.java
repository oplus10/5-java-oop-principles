package com.example.task04;

public class Task04Main {
    public static void main(String[] args) {
        Logger logger = new Logger("logger1");

        logger.addHandler(new ConsoleHandler());
        logger.addHandler(new RotationFileHandler("log0.log", 10));
        logger.log(Log.Level.ERROR, "test message :D");
    }
}
