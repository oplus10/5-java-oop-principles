package com.example.task04;

public interface MessageHandler {
    void handleLogMessage(String formattedMessage);
}
